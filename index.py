from django.shortcuts import render
from django.http import JsonResponse
from django.core.paginator import Paginator  # Django内置分页功能模块

from product.objs.product import Product
from system.objs.system_dict import System_dict
import json

# Create your views here.
levelList = System_dict().system_result_dict('product_level')
departmentList = System_dict().system_result_dict('product_dept')


def index(request):
    """产品管理页面"""
    if request.method == "POST":
        request.session.setdefault('department', request.POST.get('department'))
        request.session.setdefault('level', request.POST.get('level'))

    return render(request, 'product/product.html', {'level': levelList, 'dept': departmentList})


def create(request):
    """新增产品跳转页面"""
    return render(request, 'product/product_create.html', {'level': levelList, 'dept': departmentList})


def product_list(request):
    """产品下拉选择框列表"""
    pro_dict = Product().product_list()
    return JsonResponse(pro_dict)


def create_product(request):
    """产品管理页面的创建功能"""
    form_data = json.loads(request.POST.get("parmd_data"))
    result = Product().product_create(form_data)
    return JsonResponse({'code': result['code'], 'msg': result['msg']})


def table_product_list(request):
    """产品管理首页的table表格数据列表"""
    param = {}
    if request.POST.get('level', '') != '':
        param['level'] = request.POST.get('level', '')
    if request.POST.get('dept', '') != '':
        param['department'] = request.POST.get('dept', '')

    result = Product().table_product_list(param)
    if result['code'] == 200:
        # 分页
        pageIndex = request.POST.get('page', '1')
        pageSize = request.POST.get('limit', '10')
        pageInator = Paginator(result['data'], pageSize)
        contacts = pageInator.page(pageIndex)
        data = []
        for contact in contacts:
            data.append(contact)
        return JsonResponse({'code': 0, 'msg': '查询数据成功！', 'count': result['count'], 'data': data})
    else:
        return JsonResponse({'code': result['code'], 'msg': result['msg'], 'data': result['data']})
